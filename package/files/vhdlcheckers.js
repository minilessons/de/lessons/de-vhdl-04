    function VHDLCheckerResult() {
      this.exceptionName = null;
      this.exceptionMessage = null;
      this.errorMessages = [];
      this.infoMessages = [];
    }

    VHDLCheckerResult.prototype.hasAnyErrors = function() {
      return this.exceptionName != null || this.exceptionMessage != null || this.errorMessages.length>0;
    }

    VHDLCheckerResult.prototype.hasAnyErrorsOrInfos = function() {
      return this.exceptionName != null || this.exceptionMessage != null || this.errorMessages.length>0 || this.infoMessages.length>0;
    }

    function VHDLBasicEntityChecker(res) {
      this.result = res ? res : new VHDLCheckerResult();
    }

    VHDLBasicEntityChecker.prototype.checkStdLogicIn = function(entityNode, portName) {
      let port = entityNode.getPort(portName);
      if(port==null) { this.result.errorMessages.push("Nedostaje deklaracija ulaza '"+portName+"'."); return; }
      if(port.mode != "IN") { this.result.errorMessages.push("Port '"+portName+"' mora biti deklariran kao ulazni. Trenutno je '"+port.mode+"'."); return; }
      if(port.typeName.toLowerCase() != "std_logic") { this.result.errorMessages.push("Port '"+portName+"' mora biti tipa 'std_logic'. Trenutno je '"+port.typeName+"'."); return; }
    }

    VHDLBasicEntityChecker.prototype.checkStdLogicVectorIn = function(entityNode, portName, expectedRange) {
      let port = entityNode.getPort(portName);
      if(port==null) { this.result.errorMessages.push("Nedostaje deklaracija ulaza '"+portName+"'."); return; }
      if(port.mode != "IN") { this.result.errorMessages.push("Port '"+portName+"' mora biti deklariran kao ulazni. Trenutno je '"+port.mode+"'."); return; }
      if(port.typeName.toLowerCase() != "std_logic_vector") { this.result.errorMessages.push("Port '"+portName+"' mora biti tipa 'std_logic_vector'. Trenutno je '"+port.typeName+"'."); return; }
      if(port.range.size() != expectedRange.size()) { this.result.errorMessages.push("Port '"+portName+"' mora biti vektor širine "+expectedRange.size()+" bita. Vi ste ga deklarirali kao da je širine "+port.range.size()+" bita."); return; }
      if(port.rangerangeStart < 0 || port.rangerangeEnd < 0) { this.result.errorMessages.push("Port '"+portName+"' koristi negativne brojeve u granicama definiranog raspona. Nemojte to raditi."); return; }
      if(port.range.rangeDirection != expectedRange.rangeDirection) { this.result.infoMessages.push("Bilo bi dobro da raspon bitova kod porta '"+portName+"' definiran kao '"+expectedRange.rangeDirection+"'. Vi ste ga deklarirali kao '"+port.range.rangeDirection+"'."); return; }
      if(port.range.rangeDirection===VHDLRange.DIRECTION_TO && port.range.rangeStart != 0) { this.result.infoMessages.push("Port '"+portName+"' koristi '"+port.range.rangeDirection+"' za definiciju raspona. U tom slučaju, bilo bi dobro da raspon započinje s indeksom 0. Kod Vas započinje s "+port.range.rangeStart+"."); return; }
      if(port.range.rangeDirection===VHDLRange.DIRECTION_DOWNTO && port.range.rangeEnd != 0) { this.result.infoMessages.push("Port '"+portName+"' koristi '"+port.range.rangeDirection+"' za definiciju raspona. U tom slučaju, bilo bi dobro da raspon završava s indeksom 0. Kod Vas završava s "+port.range.rangeEnd+"."); return; }
    }

    VHDLBasicEntityChecker.prototype.checkStdLogicOut = function(entityNode, portName) {
      let port = entityNode.getPort(portName);
      if(port==null) { this.result.errorMessages.push("Nedostaje deklaracija izlaza '"+portName+"'."); return; }
      if(port.mode != "OUT") { this.result.errorMessages.push("Port '"+portName+"' mora biti deklariran kao izlazni. Trenutno je '"+port.mode+"'."); return; }
      if(port.typeName.toLowerCase() != "std_logic") { this.result.errorMessages.push("Port '"+portName+"' mora biti tipa 'std_logic'. Trenutno je '"+port.typeName+"'."); return; }
    }

    VHDLBasicEntityChecker.prototype.checkStdLogicVectorOut = function(entityNode, portName, expectedRange) {
      let port = entityNode.getPort(portName);
      if(port==null) { this.result.errorMessages.push("Nedostaje deklaracija izlaza '"+portName+"'."); return; }
      if(port.mode != "OUT") { this.result.errorMessages.push("Port '"+portName+"' mora biti deklariran kao izlazni. Trenutno je '"+port.mode+"'."); return; }
      if(port.typeName.toLowerCase() != "std_logic_vector") { this.result.errorMessages.push("Port '"+portName+"' mora biti tipa 'std_logic_vector'. Trenutno je '"+port.typeName+"'."); return; }
      if(port.range.size() != expectedRange.size()) { this.result.errorMessages.push("Port '"+portName+"' mora biti vektor širine "+expectedRange.size()+" bita. Vi ste ga deklarirali kao da je širine "+port.range.size()+" bita."); return; }
      if(port.rangerangeStart < 0 || port.rangerangeEnd < 0) { this.result.errorMessages.push("Port '"+portName+"' koristi negativne brojeve u granicama definiranog raspona. Nemojte to raditi."); return; }
      if(port.range.rangeDirection != expectedRange.rangeDirection) { this.result.infoMessages.push("Bilo bi dobro da raspon bitova kod porta '"+portName+"' definiran kao '"+expectedRange.rangeDirection+"'. Vi ste ga deklarirali kao '"+port.range.rangeDirection+"'."); return; }
      if(port.range.rangeDirection===VHDLRange.DIRECTION_TO && port.range.rangeStart != 0) { this.result.infoMessages.push("Port '"+portName+"' koristi '"+port.range.rangeDirection+"' za definiciju raspona. U tom slučaju, bilo bi dobro da raspon započinje s indeksom 0. Kod Vas započinje s "+port.range.rangeStart+"."); return; }
      if(port.range.rangeDirection===VHDLRange.DIRECTION_DOWNTO && port.range.rangeEnd != 0) { this.result.infoMessages.push("Port '"+portName+"' koristi '"+port.range.rangeDirection+"' za definiciju raspona. U tom slučaju, bilo bi dobro da raspon završava s indeksom 0. Kod Vas završava s "+port.range.rangeEnd+"."); return; }
    }

    VHDLBasicEntityChecker.prototype.compileUnneededPorts = function(entityNode, expectedPorts) {
      if(entityNode.ports==null) return [];
      let ports = entityNode.ports;
      let res = [];
      ports.forEach(function(p) {if(undefined===expectedPorts.find(function(n) {return n.toLowerCase()===p.name.toLowerCase();})) res.push(p.name);});
      return res;
    }

    VHDLBasicEntityChecker.prototype.checkExpectedPortOrder = function(entityNode, expectedPorts) {
      let ports = entityNode.ports;
      if(ports==null || expectedPorts==null) return false;
      if(ports.length != expectedPorts.length) return false;
      for(let i = 0; i < ports.length; i++) {
        if(ports[i].name.toLowerCase() != expectedPorts[i].toLowerCase()) return true;
      }
      return false;
    }

    function VHDLCheckerTools() {
    }

    VHDLCheckerTools.prototype.buildVHDLChecker = function(text, parseCall) {
      var result = new VHDLCheckerResult();
      var parser, node;
      try {
        parser = new VHDLParser(text);
        node = parseCall.apply(null, [parser]);
        parser.verifyInputConsumed();
      } catch(err) {
        result.exceptionName = err.name;
        result.exceptionMessage = err.message;
      }
      return {result: result, node: node, parser: parser};
    }

    VHDLCheckerTools.prototype.checkArrayEntriesUnique = function(arr) {
      for(let i = 0; i < arr.length; i++) {
        let v1 = arr[i].toLowerCase();
        for(let j = i+1; j < arr.length; j++) {
          let v2 = arr[j].toLowerCase();
          if(v1===v2) return false;
        }
      }
      return true;
    }

    VHDLCheckerTools.prototype.findArrayEntriesMissing = function(arrTemplate, arrToCheck) {
      let res = [];
      for(let i = 0; i < arrTemplate.length; i++) {
        let v1 = arrTemplate[i].toLowerCase();
        let found = false;
        for(let j = 0; j < arrToCheck.length; j++) {
          let v2 = arrToCheck[j].toLowerCase();
          if(v1===v2) { found = true; break; }
        }
        if(!found) {
          res.push(arrTemplate[i]);
        }
      }
      return res;
    }

    VHDLCheckerTools.prototype.checkLibUses = function(result, libsArray, usesArray, expectedLibs, expectedUses) {
      let libsUnique = this.checkArrayEntriesUnique(libsArray);
      if(!libsUnique) {
        result.errorMessages.push("Više puta ste uključili istu biblioteku. Ispravite to.");
        return;
      }
      let missing = this.findArrayEntriesMissing(expectedLibs, libsArray);
      let extra = this.findArrayEntriesMissing(libsArray, expectedLibs);
      let err = false;
      if(missing.length>0) {
        missing.forEach(function(e) {
          result.errorMessages.push("Niste uključili biblioteku '"+e+"'. Ispravite to.");
        });
      }
      if(extra.length>0) {
        extra.forEach(function(e) {
          result.errorMessages.push("Uključili ste biblioteku '"+e+"' ali to nije trebalo. Ispravite to.");
        });
      }
      let givenUses = usesArray.map(function(e) {return e.join(".");});
      let expUses = expectedUses.map(function(e) {return e.join(".");});

      if(!this.checkArrayEntriesUnique(givenUses)) {
        result.errorMessages.push("Više puta ste napisali uključivanje istih elemenata iz biblioteka. Ispravite to.");
        return;
      }

      let missing2 = this.findArrayEntriesMissing(expUses, givenUses);
      let extra2 = this.findArrayEntriesMissing(givenUses, expUses);

      if(missing2.length>0) {
        missing2.forEach(function(e) {
          let v = e.split(".");
          if(v.length===3 && v[2].toLowerCase()=="all") {
            result.errorMessages.push("Niste iz biblioteke '"+v[0]+"' iz paketa '"+v[1]+"' uključili sve elemente. To možete uporabom ključne riječi 'all' na mjestu na kojem inače dođe ime elementa kojeg želite uključiti.");
          } else {
            result.errorMessages.push("Niste iz biblioteke '"+v[0]+"' iz paketa '"+v[1]+"' uključili element '"+v[2]+"'. Ispravite to.");
          }
        });
      }
      if(extra2.length>0) {
        extra2.forEach(function(e) {
          result.errorMessages.push("Definirali ste uključivanje '"+e+"', ali to nije trebalo. Ispravite to.");
        });
      }
    }

    VHDLCheckerTools.prototype.compareWithTargetEntity = function(result, node, targetEntity, checkOrdering) {
      if(result.exceptionName != null) return;
      let checker = new VHDLBasicEntityChecker(result);

      if(node.ports==null && targetEntity.ports!=null) {
        checker.result.errorMessages.push("Napisali ste deklaraciju sučelja koja ne definira niti jedan port. Deklaraciju portova radite tako da nakon 'ENTITY "+node.entityName+" IS' umetnete blok 'PORT ();' te između oblih zagrada definirate ulaze i izlaze.");
      }
      let orderedNames = [];
      for(let tindex = 0; tindex < targetEntity.ports.length; tindex++) {
        let tport = targetEntity.ports[tindex];
        orderedNames.push(tport.name);
        if(tport.mode=="IN") {
          if(tport.typeName.toLowerCase()=="std_logic") {
            checker.checkStdLogicIn(node, tport.name);
          } else if(tport.typeName.toLowerCase()=="std_logic_vector") {
            checker.checkStdLogicVectorIn(node, tport.name, tport.range);
          } else {
            checker.result.errorMessages.push("Target interface contains unsupported type: '"+tport.typeName+"'.");
          }
        } else if(tport.mode=="OUT") {
          if(tport.typeName.toLowerCase()=="std_logic") {
            checker.checkStdLogicOut(node, tport.name);
          } else if(tport.typeName.toLowerCase()=="std_logic_vector") {
            checker.checkStdLogicVectorOut(node, tport.name, tport.range);
          } else {
            checker.result.errorMessages.push("Target interface contains unsupported type: '"+tport.typeName+"'.");
          }
        } else {
          checker.result.errorMessages.push("Target interface contains port of unsupported mode: '"+tport.mode+"'.");
        }
      }

      let nep = checker.compileUnneededPorts(node, orderedNames);
      if(nep.length>0) checker.result.errorMessages.push("Deklaracija sučelja navodi signale koji su nepotrebni: "+nep.join(", ")+".");
      if(node.entityName.toLowerCase() != targetEntity.entityName.toLowerCase()) {
        checker.result.errorMessages.push("Naveli ste da se sklop za koji opisujete sučelje zove '"+node.entityName+"'. Ispravan naziv sklopa morao bi biti '"+targetEntity.entityName+"'.");
      }
      if(checker.result.errorMessages.length==0) {
        if(checkOrdering) {
          if(checker.checkExpectedPortOrder(node,orderedNames)) {
            checker.result.infoMessages.push("Bilo bi poželjno da portove poredate drugačijim redosljedom: " + orderedNames.join(", ")+".");
          }
        }
      }
    }

    VHDLCheckerTools.prototype.generateHTMLResultRepresentation = function(result, izlaz) {
      if(result.exceptionName != null) {
        document.getElementById(izlaz).innerHTML = "<strong>Napisani kod ne može se parsirati. Prijavljena je sljedeće.</strong><br>Tip pogreške: <em>"+result.exceptionName+"</em><br>Tekst pogreške: <em>"+result.exceptionMessage+"</em>";
        return;
      }
      let dodatak = result.infoMessages.length==0 ? "" : "<br><strong>Savjet</strong>:<br>"+result.infoMessages.join("<br>");
      if(result.errorMessages.length==0) {
        document.getElementById(izlaz).innerHTML = "<strong>Unos je ispravan.</strong>"+dodatak;
      } else {
        let buf = "<strong>Postoje pogreške. U nastavku je prikazan njihov popis.</strong><br>"+result.errorMessages.join("<br>");
        document.getElementById(izlaz).innerHTML = buf + dodatak;
      }
    }

    VHDLCheckerTools.prototype.portIterate = function(node, f) {
      let ports = node.entity.ports;
      if(ports != null) {
        for(let i = 0; i < ports.length; i++) {
          f.apply(null, [ports[i]]);
        }
      }
    }

    VHDLCheckerTools.prototype.definitionIterate = function(node, f) {
      let definitions = node.architecture.definitions;
      if(definitions != null) {
        for(let i = 0; i < definitions.length; i++) {
          f.apply(null, [definitions[i]]);
        }
      }
    }

    VHDLCheckerTools.prototype.concCommandsIterate = function(node, f) {
      if(node.architecture.statements===null) return;
      let commands = node.architecture.statements.statements;
      if(commands != null) {
        for(let i = 0; i < commands.length; i++) {
          f.apply(null, [commands[i]]);
        }
      }
    }

    /* Argument je design-unit. Provjerava da isto ime nije korišteno više puta za različite stvari. 
       Vraća true ako ima pogrešaka, false inače.
     */
    VHDLCheckerTools.prototype.checkNoDuplicateNames = function(result, node) {
      let res = [];
      let errors = false;
      this.portIterate(node, function(p) {
        let nm = p.name.toLowerCase();
        if(res.indexOf(nm) != -1) { 
          errors = true; 
          result.errorMessages.push("'"+p.name+"' je deklariran više puta. To nije dozvoljeno.");
        } else {
          res.push(nm);
        }
      });
      this.definitionIterate(node, function(d) {
        if(d.nodeType===VHDLParser.TN_SIGCONST_DECLARATION) {
          let nm = d.name.toLowerCase();
          if(res.indexOf(nm) != -1) { 
            errors = true; 
            result.errorMessages.push("'"+d.name+"' je deklariran više puta. To nije dozvoljeno.");
          } else {
            res.push(nm);
          }
        }
      });
      return errors;
    }

    /* Argument je rezultat poziva VHDLCheckerTools.extractUsableRWNames(node).
       Vraća true ako predano ime postoji; vraća false inače. */
    VHDLCheckerTools.prototype.checkNameIsValid = function(usableNames, name) {
      let tn = name.toLowerCase();
      for(let i = 0; i < usableNames.length; i++) {
        let n = usableNames[i];
        if(n.name === tn) {
          return true;
        }
      }
      return false;
    }

    /* Argument je rezultat poziva VHDLCheckerTools.extractUsableRWNames(node).
       Vraća true ako se u predano ime može pisati; vraća false inače. */
    VHDLCheckerTools.prototype.checkNameIsWritable = function(usableNames, name) {
      let tn = name.toLowerCase();
      for(let i = 0; i < usableNames.length; i++) {
        let n = usableNames[i];
        if(n.name === tn) {
          return n.writable;
        }
      }
      return false;
    }

    /* Argument je rezultat poziva VHDLCheckerTools.extractUsableRWNames(node).
       Vraća true ako se iz predanog imena može čitati; vraća false inače. */
    VHDLCheckerTools.prototype.checkNameIsReadable = function(usableNames, name) {
      let tn = name.toLowerCase();
      for(let i = 0; i < usableNames.length; i++) {
        let n = usableNames[i];
        if(n.name === tn) {
          return n.readable;
        }
      }
      return false;
    }

    /* Argument je design-unit. Vadi sve izlaze kao i definirane interne signale. */
    VHDLCheckerTools.prototype.extractUsableRWNames = function(node) {
      let res = [];
      this.portIterate(node, function(p) {
        let nm = p.name.toLowerCase();
        res.push({
          name: nm, 
          typeName: p.typeName, 
          typeRange: p.range, 
          readable: p.mode===VHDLPort.MODE_IN || p.mode===VHDLPort.MODE_INOUT || p.mode===VHDLPort.MODE_BUFFER,
          writable: p.mode===VHDLPort.MODE_OUT || p.mode===VHDLPort.MODE_INOUT || p.mode===VHDLPort.MODE_BUFFER
        });
      });
      this.definitionIterate(node, function(d) {
        if(d.nodeType===VHDLParser.TN_SIGCONST_DECLARATION) {
          let rd = d.kind===VHDLParser.SC_SIGNAL || d.kind===VHDLParser.SC_CONSTANT || d.kind===VHDLParser.SC_VARIABLE;
          let wr = d.kind===VHDLParser.SC_SIGNAL;
          let nm = d.name.toLowerCase();
          let tn = null, tr = null;
          if(d.typeSpec.nodeType===VHDLParser.TN_RANGED_NAME) {
            tn = d.typeSpec.name;
            tr = d.typeSpec.range;
          } else if(d.typeSpec.nodeType===VHDLParser.TN_SIMPLE_NAME) {
            tn = d.typeSpec.name;
            tr = null;
          } else {
            throw {name: "ModelCheckingError", message: "Pronađena nepodržana vrsta imena u deklaracijskom dijelu arhitekture."};
          }
          res.push({
            name: nm, 
            typeName: tn, 
            typeRange: tr, 
            readable: rd,
            writable: wr
          });
        }
      });
      return res;
    }

