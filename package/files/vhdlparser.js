function VHDLToken(ttype, tval) {
  this.ttype = ttype;
  this.tvalue = tval;
}

function VHDLLexerError(message) {
  this.name = 'VHDLLexerError';
  this.message = message || 'Lexer error occured.';
  this.stack = (new Error()).stack;
}
VHDLLexerError.prototype = Object.create(Error.prototype);
VHDLLexerError.prototype.constructor = VHDLLexerError;

function VHDLLexer(text) {
  this.lexerState = 0;
  this.data = Array.from(text);
  this.curr = 0;
  this.dlen = this.data.length;
  this.ctoken = null;
  this.stateStack = [];
}

VHDLLexer.prototype.saveState = function() {
  let st = [this.lexerState, this.curr, this.ctoken];
  this.stateStack.push(st);
}

VHDLLexer.prototype.restoreState = function() {
  let st = this.stateStack.pop();
  this.lexerState = st[0];
  this.curr = st[1];
  this.ctoken = st[2];
}

VHDLLexer.prototype.dropState = function() {
  this.stateStack.pop();
}

VHDLLexer.prototype.TT_EOF = 0;
VHDLLexer.prototype.TT_IDENTIFIER = 1;
VHDLLexer.prototype.TT_SYMBOL = 2;
VHDLLexer.prototype.TT_OPERATOR = 3;
VHDLLexer.prototype.TT_SELECTED_NAME = 4;
VHDLLexer.prototype.TT_CHAR_LITERAL = 5;
VHDLLexer.prototype.TT_STRING_LITERAL = 6;
VHDLLexer.prototype.TT_INTEGER_LITERAL = 7;
VHDLLexer.prototype.TT_DECIMAL_LITERAL = 8;

VHDLLexer.prototype.SYMBOLS = [':', ';', '(', ')', ',', '&', '+', '-', '*', '/'];
VHDLLexer.prototype.OPERATORS = [':=', '<=', '>=', '=>', '/=', '=', '<', '>', '**'];

VHDLLexer.prototype.KW_ARRAY = ['abs', 'access', 'after', 'alias', 'all', 'and', 'architecture', 'array', 'assert', 'attribute', 'begin', 'block', 'body', 'buffer', 'bus', 'case', 'component', 'configuration', 'constant', 'disconnect', 'downto', 'else', 'elsif', 'end', 'entity', 'exit', 'file', 'for', 'function', 'generate', 'generic', 'guarded', 'if', 'impure  ', 'in', 'inertial', 'inout', 'is', 'label', 'library', 'linkage', 'literal', 'loop', 'map', 'mod', 'nand', 'new', 'next', 'nor', 'not', 'null', 'of', 'on', 'open', 'or', 'others', 'out', 'package', 'port', 'postponed', 'procedure', 'process', 'pure', 'range', 'record', 'register', 'reject', 'rem', 'report', 'return', 'rol', 'ror', 'select', 'severity', 'shared', 'signal', 'sla  ', 'sll', 'sra', 'srl', 'subtype', 'then', 'to', 'transport', 'type', 'unaffected', 'units', 'until', 'use', 'variable', 'wait', 'when', 'while', 'with', 'xnor', 'xor'];

VHDLLexer.prototype.isKeyword = function(ident) {
  return this.KW_ARRAY.indexOf(ident.toLowerCase()) != -1;
}

VHDLLexer.prototype.checkTokenIsKeyword = function() {
  if(this.ctoken==null || this.ctoken.ttype != this.TT_IDENTIFIER) return false;
  return this.KW_ARRAY.indexOf(this.ctoken.tvalue.toLowerCase()) != -1;
}

VHDLLexer.prototype.isTokenKeyword = function(keywordName) {
  if(this.ctoken==null || this.ctoken.ttype != this.TT_IDENTIFIER) return false;
  return this.ctoken.tvalue.toLowerCase() == keywordName;
}

VHDLLexer.prototype.isTokenSymbol = function(sym) {
  if(this.ctoken==null || this.ctoken.ttype != this.TT_SYMBOL) return false;
  return this.ctoken.tvalue === sym;
}

VHDLLexer.prototype.isTokenOperator = function(oper) {
  if(this.ctoken==null || this.ctoken.ttype != this.TT_OPERATOR) return false;
  return this.ctoken.tvalue === oper;
}

VHDLLexer.prototype.startsWith = function(data, pos, str, strlen) {
  for(var i = 0; i < strlen; i++) {
    if(data[pos+i] != str[i]) return false;
  }
  return true;
}

VHDLLexer.prototype.isOperator = function() {
  var o = this.OPERATORS;
  var l = o.length;
  for(var i = 0; i < l; i++) {
    var v = o[i];
    var vl = v.length;
    if(this.curr+vl-1 < this.dlen && this.startsWith(this.data, this.curr, v, vl)) return v;
  }
  return null;
}

VHDLLexer.prototype.convertToDigit = function(c, base) {
  let dig = 0;
  let cc = c.charCodeAt(0);
  if(cc>=48 && cc<=57) {
    dig = cc-48;
  } else if(cc>=65 && cc<=70) {
    dig =  cc-65+10;
  } else if(cc>=97 && cc<=102) {
    dig = cc-97+10;
  } else {
    throw new VHDLLexerError("'"+c+"' is not valid digit in base "+base+".");
  }
  if(dig >= base) {
    throw new VHDLLexerError("'"+c+"' is not valid digit in base "+base+".");
  }
  return dig;
}

VHDLLexer.prototype.isBasedDigit = function(c, base) {
  let dig = 0;
  let cc = c.charCodeAt(0);
  if(cc>=48 && cc<=57) {
    dig = cc-48;
  } else if(cc>=65 && cc<=70) {
    dig =  cc-65+10;
  } else if(cc>=97 && cc<=102) {
    dig = cc-97+10;
  } else {
    return false;
  }
  if(dig >= base) {
    return false;
  }
  return true;
}

VHDLLexer.prototype.readBasedBitValue = function(numBase) {
  let num = 0;
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  num = num*numBase + this.convertToDigit(this.data[this.curr], numBase);
  this.curr++;
  while(this.curr < this.dlen && this.data[this.curr]!='"') {
    if(this.data[this.curr]=='_') {
      this.curr++;
      if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
    }
    num = num*numBase + this.convertToDigit(this.data[this.curr], numBase);
    this.curr++;
  }
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  // Inače je navodnik što je OK;
  this.curr++;
  return num;
}

VHDLLexer.prototype.readBasedInteger = function(numBase) {
  let num = this.readBasedIntegerWhole(numBase);
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  let hasDecimal = false;
  if(this.data[this.curr]=='.') {
    this.curr++;
    num += this.readBasedIntegerDecimal(numBase);
    if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
    hasDecimal = true;
  }
  if(this.data[this.curr]!='#') {
    throw new VHDLLexerError("Expected '#' as terminator for based integer.");
  }
  this.curr++;
  if(this.curr < this.dlen && this.data[this.curr]=='E') {
    this.curr++;
    if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
    let signIsPlus = true;
    if(this.data[this.curr]=='+') {
      this.curr++;
    } else if(this.data[this.curr]=='-') {
      this.curr++;
      signIsPlus = false;
      hasDecimal = true;
    }
    let num3 = this.readInteger();
    if(!signIsPlus) num3 = -num3;
    num = num * Math.pow(numBase, num3);
  }
  return {num: num, hasDecimal: hasDecimal};
}

VHDLLexer.prototype.readBasedIntegerWhole = function(numBase) {
  let num = 0;
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  num = num*numBase + this.convertToDigit(this.data[this.curr], numBase);
  this.curr++;
  let mustFollow = false;
  while(this.curr < this.dlen) {
    mustFollow = false;
    if(this.data[this.curr]=='_') {
      this.curr++;
      if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
      mustFollow = true;
    }
    if(this.isBasedDigit(this.data[this.curr], numBase)) {
      num = num*numBase + this.convertToDigit(this.data[this.curr], numBase);
      this.curr++;
      continue;
    }
    if(mustFollow) throw new VHDLLexerError("Expected digit in number literal after '_'.");
    break;
  }
  return num;
}

VHDLLexer.prototype.readBasedIntegerDecimal = function(numBase) {
  let num = 0;
  let currentPower = 1.0/numBase;
  let currentMultiplier = currentPower;
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  num = this.convertToDigit(this.data[this.curr], numBase) * currentMultiplier;
  this.curr++;
  let mustFollow = false;
  while(this.curr < this.dlen) {
    mustFollow = false;
    if(this.data[this.curr]=='_') {
      this.curr++;
      if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
      mustFollow = true;
    }
    currentMultiplier *= currentPower;
    if(this.isBasedDigit(this.data[this.curr], numBase)) {
      num += this.convertToDigit(this.data[this.curr], numBase) * currentMultiplier;
      this.curr++;
      continue;
    }
    if(mustFollow) throw new VHDLLexerError("Expected digit in number literal after '_'.");
    break;
  }
  return num;
}

VHDLLexer.prototype.readInteger = function() {
  let num = 0;
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  num = num*10 + this.convertToDigit(this.data[this.curr], 10);
  this.curr++;
  let mustFollow = false;
  while(this.curr < this.dlen) {
    mustFollow = false;
    if(this.data[this.curr]=='_') {
      this.curr++;
      if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
      mustFollow = true;
    }
    if(this.data[this.curr]>='0' && this.data[this.curr]<='9') {
      num = num*10 + this.convertToDigit(this.data[this.curr], 10);
      this.curr++;
      continue;
    }
    if(mustFollow) throw new VHDLLexerError("Expected digit in number literal after '_'.");
    break;
  }
  return num;
}

VHDLLexer.prototype.readIntegerDecimalPart = function() {
  let num = 0;
  let currentPower = 1.0/10.0;
  let currentMultiplier = currentPower;
  if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
  num = this.convertToDigit(this.data[this.curr], 10) * currentMultiplier;
  this.curr++;
  let mustFollow = false;
  while(this.curr < this.dlen) {
    mustFollow = false;
    if(this.data[this.curr]=='_') {
      this.curr++;
      if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF.");
      mustFollow = true;
    }
    currentMultiplier *= currentPower;
    if(this.data[this.curr]>='0' && this.data[this.curr]<='9') {
      num += this.convertToDigit(this.data[this.curr], 10) * currentMultiplier;
      this.curr++;
      continue;
    }
    if(mustFollow) throw new VHDLLexerError("Expected digit in number literal after '_'.");
    break;
  }
  return num;
}

VHDLLexer.prototype.next = function() {
  function isDigit(d) {
    if(d.length != 1) return false;
    return d>='0' && d<='9';
  }
  function isLetter(d) {
    if(d.length != 1) return false;
    return (d>='a' && d<='z') || (d>='A' && d<='Z');
  }
  function isLetterOrDigit(d) {
    if(d.length != 1) return false;
    return (d>='a' && d<='z') || (d>='A' && d<='Z') || (d>='0' && d<='9');
  }
  if(this.ctoken != null && this.ctoken.ttype == this.TT_EOF) return;
  while(this.curr < this.dlen && this.data[this.curr]<=' ') this.curr++;
  if(this.curr == this.dlen) {
    this.ctoken = new VHDLToken(this.TT_EOF, "");
    return this.ctoken;
  }
  if(this.data[this.curr]=='\"') {
    let strlit = [];
    this.curr++;
    while(this.curr<this.dlen) {
      let cc = this.data[this.curr];
      if(cc=='\r' || cc=='\n') {
        throw new VHDLLexerError("Unterminated string literal found: '"+strlit.join("")+"'.");
      }
      if(cc=='\"') {
        if(this.curr+1<this.dlen && this.data[this.curr+1]=='\"') {
          strlit.push('\"');
          this.curr += 2;
          continue;
        }
        this.curr++;
        break;
      }
      strlit.push(cc);
      this.curr++;
    }
    this.ctoken = new VHDLToken(this.TT_STRING_LITERAL, strlit.join(""));
    return this.ctoken;
  }
  if(this.data[this.curr]=='\'') {
    if(this.curr+2 >= this.dlen) throw new VHDLLexerError("Invalid character literal.");
    let chlit = this.data[this.curr+1];
    if(chlit=='\'') {
      if(this.curr+3 >= this.dlen || this.data[this.curr+2]!='\'' || this.data[this.curr+3]!='\'') {
        throw new VHDLLexerError("Invalid character literal.");
      }
      this.curr += 4;
    } else {
      if(this.data[this.curr+2]!='\'') throw new VHDLLexerError("Invalid character literal.");
      this.curr += 3;
    }
    this.ctoken = new VHDLToken(this.TT_CHAR_LITERAL, chlit);
    return this.ctoken;
  }
  if(this.data[this.curr]=='-' && this.curr+1 < this.dlen && this.data[this.curr+1]=='-') {
    this.curr += 2;
    while(this.curr < this.dlen && this.data[this.curr]!='\r' && this.data[this.curr]!='\n') this.curr++;
    if(this.curr == this.dlen) {
      this.ctoken = new VHDLToken(this.TT_EOF, "");
      return this.ctoken;
    }
    this.curr++;
    return this.next();
  }
  if((this.data[this.curr]=='B'||this.data[this.curr]=='O'||this.data[this.curr]=='X') && this.curr+1 < this.dlen && this.data[this.curr+1]=='"') {
    let numBase = this.data[this.curr]=='B' ? 2 : (this.data[this.curr]=='O' ? 8 : 16);
    this.curr+=2;
    let number = this.readBasedBitValue(numBase);
    this.ctoken = new VHDLToken(this.TT_INTEGER_LITERAL, number);
    return this.ctoken;
  }
  if(this.data[this.curr]>='0' && this.data[this.curr]<='9') {
    let num1 = this.readInteger();
    if(this.curr >= this.dlen) {
      this.ctoken = new VHDLToken(this.TT_INTEGER_LITERAL, num1);
      return this.ctoken;
    }
    if(this.data[this.curr]=='#') {
      this.curr++;
      let res = this.readBasedInteger(num1);
      this.ctoken = new VHDLToken(res.hasDecimal ? this.TT_DECIMAL_LITERAL : this.TT_INTEGER_LITERAL, res.num);
      return this.ctoken;
    }
    let hasDecimal = false;
    if(this.data[this.curr]=='.') {
      this.curr++;
      let num3 = this.readIntegerDecimalPart();
      num1 += num3;
      hasDecimal = true;
      if(this.curr >= this.dlen) {
        this.ctoken = new VHDLToken(this.TT_DECIMAL_LITERAL, num1);
        return this.ctoken;
      }
    }
    if(this.data[this.curr]=='E') {
      this.curr++;
      if(this.curr >= this.dlen) throw new VHDLLexerError("Unexpected EOF in decimal literal.");
      let signIsPlus = true;
      if(this.data[this.curr]=='+') {
        this.curr++;
      } else if(this.data[this.curr]=='-') {
        this.curr++;
        signIsPlus = false;
        hasDecimal = true;
      }
      let num3 = this.readInteger();
      if(!signIsPlus) num3 = -num3;
      num1 = num1 * Math.pow(10.0, num3);
    }
    this.ctoken = new VHDLToken(hasDecimal ? this.TT_DECIMAL_LITERAL : this.TT_INTEGER_LITERAL, num1);
    return this.ctoken;
  }
  if(isLetter(this.data[this.curr])) {
    let outerBuf = [];
    while(true) {
      let buf = [this.data[this.curr]];
      this.curr++;
      while(this.curr < this.dlen) {
        if(isLetterOrDigit(this.data[this.curr])) {
          buf.push(this.data[this.curr]);
          this.curr++;
          continue;
        }
        if(this.data[this.curr]=='_') {
          if(this.curr+1 >= this.dlen) {
            throw new VHDLLexerError("Identifier can not end with underscore: "+buf.join()+"_.");
          }
          if(!isLetterOrDigit(this.data[this.curr+1])) {
            throw new VHDLLexerError("Invalid identifier: "+buf.join("")+"_"+this.data[this.curr+1]+".");
          }
          buf.push('_');
          buf.push(this.data[this.curr+1]);
          this.curr+=2;
          continue;
        }
        break;
      }
      outerBuf.push(buf.join(""));
      if(this.curr < this.dlen && this.data[this.curr]=='.') {
        this.curr++;
        if(this.curr >= this.dlen || !isLetter(this.data[this.curr])) {
          throw new VHDLLexerError("Invalid identifier start: '"+outerBuf.join(".")+".'"+".");
        }
        continue;
      }
      break;
    }
    if(outerBuf.length==1) {
      this.ctoken = new VHDLToken(this.TT_IDENTIFIER, outerBuf[0]);
    } else {
      this.ctoken = new VHDLToken(this.TT_SELECTED_NAME, outerBuf);
    }
    return this.ctoken;
  }
  let op = this.isOperator();
  if(op != null) {
    this.ctoken = new VHDLToken(this.TT_OPERATOR, op);
    this.curr += op.length;
    return this.ctoken;
  }
  if(this.SYMBOLS.indexOf(this.data[this.curr])!=-1) {
    this.ctoken = new VHDLToken(this.TT_SYMBOL, ""+this.data[this.curr]);
    this.curr++;
    return this.ctoken;
  }
  throw new VHDLLexerError("Invalid character found: '"+this.data[this.curr]+"'.");
}

function VHDLParserError(message) {
  this.name = 'VHDLParserError';
  this.message = message || 'Parser error occured.';
  this.stack = (new Error()).stack;
}
VHDLParserError.prototype = Object.create(Error.prototype);
VHDLParserError.prototype.constructor = VHDLParserError;

function VHDLParser(text) {
  this.lexer = new VHDLLexer(text);
  this.lexer.next();
}

VHDLParser.prototype.parsePortDeclaration = function() {
  if(!this.lexer.isTokenKeyword("port")) {
    throw new VHDLParserError("Entity port declaration must start with 'PORT' keyword.");
  }
  this.lexer.next();

  if(!this.lexer.isTokenSymbol('(')) {
    throw new VHDLParserError("After 'PORT' keyword in entity declaration, a '(' is expected.");
  }
  this.lexer.next();

  let ports = [];
  let iteration = 0;
  while(true) {
    if(iteration==0 && this.lexer.isTokenSymbol(')')) {
      throw new VHDLParserError("If entity has an empty interface, you must skip complete PORT declaration. 'PORT ();' is not allowed - just remove it.");
    }
    if(iteration>0 && this.lexer.isTokenSymbol(')')) {
      throw new VHDLParserError("Symbol ';' can not follow last port declaration. Remove this symbol!");
    }
    iteration++;
    if(this.lexer.isTokenKeyword("signal")) {
      this.lexer.next();
    }
    let signalNames = [];
    while(true) {
      if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
        throw new VHDLParserError("Signal name was expected.");
      }
      let signalName = this.lexer.ctoken.tvalue;
      signalNames.push(signalName);
      this.lexer.next();
      if(this.lexer.isTokenSymbol(',')) {
        this.lexer.next();
        continue;
      }
      break;
    }
    if(!this.lexer.isTokenSymbol(':')) {
      throw new VHDLParserError("After (list of) signal name(s), a ':' is expected.");
    }
    this.lexer.next();
    let portMode = VHDLPort.MODE_IN;
    if(this.lexer.isTokenKeyword("in")) {
      this.lexer.next();
    } else if(this.lexer.isTokenKeyword("out")) {
      portMode = VHDLPort.MODE_OUT;
      this.lexer.next();
    } else if(this.lexer.isTokenKeyword("inout")||this.lexer.isTokenKeyword("buffer")||this.lexer.isTokenKeyword("linkage")) {
      throw new VHDLParserException("Port modes inout/buffer/linkage are not supported by this parser.");
    }
    if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
      throw new VHDLParserError("Signal type was expected; found '"+this.lexer.ctoken.tvalue+"' instead.");
    }
    let signalTypeName = this.lexer.ctoken.tvalue;
    let signalRange = null;
    this.lexer.next();
    if(this.lexer.isTokenSymbol('(')) {
      this.lexer.next();
      if(this.lexer.ctoken.ttype != this.lexer.TT_INTEGER_LITERAL) {
        throw new VHDLParserError("Expected integer literal at range start."+this.lexer.ctoken.ttype);
      }
      let sigFrom = this.lexer.ctoken.tvalue;
      this.lexer.next();

      let rangeDirection = VHDLRange.DIRECTION_TO;
      if(this.lexer.isTokenKeyword("to")) {
        this.lexer.next();
      } else if(this.lexer.isTokenKeyword("downto")) {
        rangeDirection = VHDLRange.DIRECTION_DOWNTO;
        this.lexer.next();
      } else {
        throw new VHDLParserException("Expected 'TO' or 'DOWNTO' after first limit in range declaration.");
      }

      if(this.lexer.ctoken.ttype != this.lexer.TT_INTEGER_LITERAL) {
        throw new VHDLParserError("Expected integer literal at range end.");
      }
      let sigTo = this.lexer.ctoken.tvalue;
      this.lexer.next();

      if(rangeDirection === VHDLRange.DIRECTION_TO && sigFrom > sigTo) {
        throw new VHDLParserError("If range is declared as (x TO y), x can not be greater than y.");
      }
      if(rangeDirection === VHDLRange.DIRECTION_DOWNTO && sigFrom < sigTo) {
        throw new VHDLParserError("If range is declared as (x DOWNTO y), x can not be smaller than y.");
      }
      if(!this.lexer.isTokenSymbol(')')) {
        throw new VHDLParserError("Range declaration must end with ')'.");
      }
      this.lexer.next();
      signalRange = new VHDLRange(sigFrom, sigTo, rangeDirection);
    }
    if(signalTypeName.toLowerCase()==="std_logic") {
      if(signalRange != null) throw new VHDLParserError("Type std_logic can not have range defined.");
    } else if(signalTypeName.toLowerCase()==="std_logic_vector") {
      if(signalRange == null) throw new VHDLParserError("Type std_logic_vector must have range defined.");
    }
    for(let i = 0; i < signalNames.length; i++) {
      let currentSignalName = signalNames[i];
      for(let j = 0; j < ports.length; j++) {
        if(currentSignalName.toLowerCase() === ports[j].name.toLowerCase()) {
          throw new VHDLParserError("Port '"+currentSignalName+"' is defined multiple times.");
        }
      }
      ports.push(new VHDLPort(currentSignalName, portMode, signalTypeName, signalRange));
    }
    if(this.lexer.isTokenSymbol(';')) {
      this.lexer.next();
      continue;
    }
    break;
  }
  if(!this.lexer.isTokenSymbol(')')) {
    throw new VHDLParserError("Port declaration must end with ');'.");
  }
  this.lexer.next();
  if(!this.lexer.isTokenSymbol(';')) {
    throw new VHDLParserError("Port declaration must end with ');'.");
  }
  this.lexer.next();
  return ports;
}

VHDLParser.prototype.verifyInputConsumed = function() {
  if(this.lexer.ctoken.ttype != this.lexer.TT_EOF) {
    throw new VHDLParserError("Unexpected tokens found. EOF was expected.");
  }
}

VHDLParser.TN_SIMPLE_NAME = "simple_name";
VHDLParser.TN_INDEXED_NAME = "indexed_name";
VHDLParser.TN_RANGED_NAME = "ranged_name";
VHDLParser.TN_SIMPLE_AGGREGATION = "simple_aggregation";
VHDLParser.TN_BINARY_LOGICAL_OPERATOR = "binary_logical_operator";
VHDLParser.TN_COMPARISON_OPERATOR = "comparison_operator";
VHDLParser.TN_SHIFT_OPERATOR = "shift_operator";
VHDLParser.TN_ADDING_OPERATOR = "adding_operator";
VHDLParser.TN_UNARY_OPERATOR = "unary_operator";
VHDLParser.TN_MULTIPLYING_OPERATOR = "multiplying_operator";
VHDLParser.TN_INTEGER_LITERAL = "integer_literal";
VHDLParser.TN_DECIMAL_LITERAL = "decimal_literal";
VHDLParser.TN_CHARACTER_LITERAL = "character_literal";
VHDLParser.TN_STRING_LITERAL = "string_literal";
VHDLParser.TN_WAVEFORM_ELEMENT = "waveform_element";
VHDLParser.TN_CONDITIONAL_SIGNAL_ASSIGNMENT = "cond_signal_assignment";
VHDLParser.TN_CONCURRENT_STATEMENTS = "concurrent_statements";
VHDLParser.TN_CONDITIONAL_WAVEFORM = "conditional_waveform";
VHDLParser.TN_ARCHITECTURE = "architecture";
VHDLParser.TN_DESIGN_UNIT = "design_unit";
VHDLParser.TN_SIGCONST_DECLARATION = "sigconst_declaration";
VHDLParser.TN_COMPONENT_INST = "component_instantiation";
VHDLParser.TN_OPEN_KEYWORD = "open_keyword";

VHDLParser.SC_SIGNAL = "signal";
VHDLParser.SC_CONSTANT = "constant";
VHDLParser.SC_VARIABLE = "variable";

function VHDLTNSimpleAggregation(elements) {
  this.elements = elements;
  this.nodeType = VHDLParser.TN_SIMPLE_AGGREGATION;
  let me = this;
  this.accept = function(v) { if(v.visitSimpleAggregation) {v.visitSimpleAggregation(me);} else {v.visitDefault(me);} }
}
function VHDLTNSimpleName(name) {
  this.name = name;
  this.nodeType = VHDLParser.TN_SIMPLE_NAME;
  let me = this;
  this.accept = function(v) { if(v.visitSimpleName) {v.visitSimpleName(me);} else {v.visitDefault(me);} }
}
function VHDLTNIndexedName(name,index) {
  this.name = name;
  this.index = index;
  this.nodeType = VHDLParser.TN_INDEXED_NAME;
  let me = this;
  this.accept = function(v) { if(v.visitIndexedName) {v.visitIndexedName(me);} else {v.visitDefault(me);} }
}
function VHDLTNRangedName(name,range) {
  this.name = name;
  this.range = range;
  this.nodeType = VHDLParser.TN_RANGED_NAME;
  let me = this;
  this.accept = function(v) { if(v.visitRangedName) {v.visitRangedName(me);} else {v.visitDefault(me);} }
}
function VHDLBinaryLogicalOperator(operName, children, associative) {
  this.operName = operName.toLowerCase();
  this.children = children;
  this.associative = associative;
  this.nodeType = VHDLParser.TN_BINARY_LOGICAL_OPERATOR;
  let me = this;
  this.accept = function(v) { if(v.visitBinaryLogicalOperator) {v.visitBinaryLogicalOperator(me);} else {v.visitDefault(me);} }
}
function VHDLComparisonOperator(operName, children) {
  this.operName = operName.toLowerCase();
  this.children = children;
  this.nodeType = VHDLParser.TN_COMPARISON_OPERATOR;
  let me = this;
  this.accept = function(v) { if(v.visitComparisonOperator) {v.visitComparisonOperator(me);} else {v.visitDefault(me);} }
}
function VHDLShiftOperator(operName, children) {
  this.operName = operName.toLowerCase();
  this.children = children;
  this.nodeType = VHDLParser.TN_SHIFT_OPERATOR;
  let me = this;
  this.accept = function(v) { if(v.visitShiftOperator) {v.visitShiftOperator(me);} else {v.visitDefault(me);} }
}
function VHDLAddingOperator(operName, child1, child2) {
  this.operName = operName.toLowerCase();
  this.child1 = child1;
  this.child2 = child2;
  this.nodeType = VHDLParser.TN_ADDING_OPERATOR;
  let me = this;
  this.accept = function(v) { if(v.visitAddingOperator) {v.visitAddingOperator(me);} else {v.visitDefault(me);} }
}
function VHDLUnaryOperator(operName, child) {
  this.operName = operName.toLowerCase();
  this.child = child;
  this.nodeType = VHDLParser.TN_UNARY_OPERATOR;
  let me = this;
  this.accept = function(v) { if(v.visitUnaryOperator) {v.visitUnaryOperator(me);} else {v.visitDefault(me);} }
}
function VHDLMultiplyingOperator(operName, child1, child2) {
  this.operName = operName.toLowerCase();
  this.child1 = child1;
  this.child2 = child2;
  this.nodeType = VHDLParser.TN_MULTIPLYING_OPERATOR;
  let me = this;
  this.accept = function(v) { if(v.visitMultiplyingOperator) {v.visitMultiplyingOperator(me);} else {v.visitDefault(me);} }
}
function VHDLIntegerLiteral(lit) {
  this.literal = lit;
  this.nodeType = VHDLParser.TN_INTEGER_LITERAL;
  let me = this;
  this.accept = function(v) { if(v.visitIntegerLiteral) {v.visitIntegerLiteral(me);} else {v.visitDefault(me);} }
}
function VHDLDecimalLiteral(lit) {
  this.literal = lit;
  this.nodeType = VHDLParser.TN_DECIMAL_LITERAL;
  let me = this;
  this.accept = function(v) { if(v.visitDecimalLiteral) {v.visitDecimalLiteral(me);} else {v.visitDefault(me);} }
}
function VHDLCharacterLiteral(lit) {
  this.literal = lit;
  this.nodeType = VHDLParser.TN_CHARACTER_LITERAL;
  let me = this;
  this.accept = function(v) { if(v.visitCharacterLiteral) {v.visitCharacterLiteral(me);} else {v.visitDefault(me);} }
}
function VHDLStringLiteral(lit) {
  this.literal = lit;
  this.nodeType = VHDLParser.TN_STRING_LITERAL;
  let me = this;
  this.accept = function(v) { if(v.visitStringLiteral) {v.visitStringLiteral(me);} else {v.visitDefault(me);} }
}
function VHDLWaveformElement(expression, delay) {
  this.expression = expression;
  this.delay = delay;
  this.nodeType = VHDLParser.TN_WAVEFORM_ELEMENT;
  let me = this;
  this.accept = function(v) { if(v.visitWaveformElement) {v.visitWaveformElement(me);} else {v.visitDefault(me);} }
}
function VHDLConditionalWaveform(waveform, condition) {
  this.waveform = waveform;
  this.condition = condition;
  this.nodeType = VHDLParser.TN_CONDITIONAL_WAVEFORM;
  let me = this;
  this.accept = function(v) { if(v.visitConditionalWaveform) {v.visitConditionalWaveform(me);} else {v.visitDefault(me);} }
}
function VHDLConditionalSignalAssignment(label, target, waveforms) {
  this.label = label;
  this.target = target;
  this.waveforms = waveforms;
  this.nodeType = VHDLParser.TN_CONDITIONAL_SIGNAL_ASSIGNMENT;
  let me = this;
  this.accept = function(v) { if(v.visitConditionalSignalAssignment) {v.visitConditionalSignalAssignment(me);} else {v.visitDefault(me);} }
}
function VHDLConcurrentStatements(statements) {
  this.statements = statements;
  this.nodeType = VHDLParser.TN_CONCURRENT_STATEMENTS;
  let me = this;
  this.accept = function(v) { if(v.visitConcurrentStatements) {v.visitConcurrentStatements(me);} else {v.visitDefault(me);} }
}
function VHDLArchitecture(archName, entityName, definitions, statements) {
  this.archName = archName;
  this.entityName = entityName;
  this.definitions = definitions;
  this.statements = statements;
  this.nodeType = VHDLParser.TN_ARCHITECTURE;
  let me = this;
  this.accept = function(v) { if(v.visitArchitecture) {v.visitArchitecture(me);} else {v.visitDefault(me);} }
}
function VHDLDesignUnit(libNames, useNames, entity, architecture) {
  this.libNames = libNames;
  this.useNames = useNames;
  this.entity = entity;
  this.architecture = architecture;
  this.nodeType = VHDLParser.TN_DESIGN_UNIT;
  let me = this;
  this.accept = function(v) { if(v.visitDesignUnit) {v.visitDesignUnit(me);} else {v.visitDefault(me);} }
}
function VHDLSigConstDeclaration(kind, name, typeSpec, initializer) {
  this.kind = kind;
  this.name = name; 
  this.typeSpec = typeSpec;
  this.initializer = initializer;
  this.nodeType = VHDLParser.TN_SIGCONST_DECLARATION;
  let me = this;
  this.accept = function(v) { if(v.visitSigConstDeclaration) {v.visitSigConstDeclaration(me);} else {v.visitDefault(me);} }
}

function VHDLCompInstStatement(/*string*/ label, /*string*/ entityName, /*array*/ portMap) {
  this.label = label;
  this.entityName = entityName;
  this.portMap = portMap;
  this.nodeType = VHDLParser.TN_COMPONENT_INST;
  let me = this;
  this.accept = function(v) { if(v.visitCompInstStatement) {v.visitCompInstStatement(me);} else {v.visitDefault(me);} }
}

function VHDLOpenKeyword() {
  this.nodeType = VHDLParser.TN_OPEN_KEYWORD;
  let me = this;
  this.accept = function(v) { if(v.visitOpenKeyword) {v.visitOpenKeyword(me);} else {v.visitDefault(me);} }
}

VHDLParser.prototype.tryReadSimpleAggregation = function() {
  if(!this.lexer.isTokenSymbol('(')) {
    return null;
  }
  this.lexer.saveState();
  this.lexer.next();
  let elements = [];
  while(true) {
    let elem = this.tryReadName();
    if(elem == null) {
      this.lexer.restoreState();
      return null;
    }
    elements.push(elem);
    if(!this.lexer.isTokenSymbol(',')) {
      break;
    }
    this.lexer.next();
  }
  if(!this.lexer.isTokenSymbol(')')) {
    this.lexer.restoreState();
    return null;
  }
  this.lexer.next();
  this.lexer.dropState();
  return new VHDLTNSimpleAggregation(elements);
}

VHDLParser.prototype.tryReadName = function() {
  if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER || this.lexer.checkTokenIsKeyword()) {
    return null;
  }
  this.lexer.saveState();
  let ident = this.lexer.ctoken.tvalue;
  this.lexer.next();
  if(!this.lexer.isTokenSymbol('(')) {
    this.lexer.dropState();
    return new VHDLTNSimpleName(ident);
  }
  this.lexer.next();
  // Sad imamo ili jedan index ili raspon:
  if(this.lexer.ctoken.ttype != this.lexer.TT_INTEGER_LITERAL) {
    this.lexer.dropState();
    return null;
  }
  let num1 = this.lexer.ctoken.tvalue;
  this.lexer.next();
  if(this.lexer.isTokenSymbol(')')) {
    this.lexer.dropState();
    this.lexer.next();
    return new VHDLTNIndexedName(ident, num1);
  }

  let rangeDirection = VHDLRange.DIRECTION_TO;
  if(this.lexer.isTokenKeyword("to")) {
    this.lexer.next();
  } else if(this.lexer.isTokenKeyword("downto")) {
    rangeDirection = VHDLRange.DIRECTION_DOWNTO;
    this.lexer.next();
  } else {
    this.lexer.dropState();
    return null;
  }

  if(this.lexer.ctoken.ttype != this.lexer.TT_INTEGER_LITERAL) {
    throw new VHDLParserError("Expected integer literal at range end.");
  }
  let sigFrom = num1;
  let sigTo = this.lexer.ctoken.tvalue;
  this.lexer.next();

  if(rangeDirection === VHDLRange.DIRECTION_TO && sigFrom > sigTo) {
    throw new VHDLParserError("If range is declared as (x TO y), x can not be greater than y.");
  }
  if(rangeDirection === VHDLRange.DIRECTION_DOWNTO && sigFrom < sigTo) {
    throw new VHDLParserError("If range is declared as (x DOWNTO y), x can not be smaller than y.");
  }
  if(!this.lexer.isTokenSymbol(')')) {
    throw new VHDLParserError("Range declaration must end with ')'.");
  }
  this.lexer.next();
  signalRange = new VHDLRange(sigFrom, sigTo, rangeDirection);

  this.lexer.dropState();
  return new VHDLTNRangedName(ident, signalRange);
}

VHDLParser.prototype.parseWaveformElement = function() {
  let ex = this.parseExpression();
  if(this.lexer.isTokenKeyword('after')) {
    this.lexer.next();
    let timeValue = 1;
    if(this.lexer.ctoken.ttype == this.lexer.TT_INTEGER_LITERAL) {
      timeValue = this.lexer.ctoken.tvalue;
      this.lexer.next();
    } else if(this.lexer.ctoken.ttype == this.lexer.TT_DECIMAL_LITERAL) {
      timeValue = this.lexer.ctoken.tvalue;
      this.lexer.next();
    }
    if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
      throw new VHDLParserError("Expected time unit. Found: '"+this.lexer.ctoken.tvalue+"'.");
    }
    let tu = this.lexer.ctoken.tvalue.toLowerCase();
    let scaler = null;
    switch(tu) {
      case "fs": scaler = 1.0; break;
      case "ps": scaler = Math.pow(10, 3); break;
      case "ns": scaler = Math.pow(10, 6); break;
      case "us": scaler = Math.pow(10, 9); break;
      case "ms": scaler = Math.pow(10, 12); break;
      case "sec": scaler = Math.pow(10, 15); break;
      case "min": scaler = 6*Math.pow(10, 16); break;
      case "hr": scaler = 36*Math.pow(10, 17); break;
      default: throw new VHDLParserError("Expected time unit. Found: '"+tu+"'.");
    }
    this.lexer.next();
    return new VHDLWaveformElement(ex, timeValue * scaler);
  }
  return new VHDLWaveformElement(ex, null);
}

VHDLParser.prototype.parseWaveform = function() {
  let we = this.parseWaveformElement();
  return we;
}

VHDLParser.prototype.parsePrimary = function() {
  if(this.lexer.ctoken.ttype == this.lexer.TT_INTEGER_LITERAL) {
    let val = this.lexer.ctoken.tvalue;
    this.lexer.next();
    return new VHDLIntegerLiteral(val);
  }
  if(this.lexer.ctoken.ttype == this.lexer.TT_DECIMAL_LITERAL) {
    let val = this.lexer.ctoken.tvalue;
    this.lexer.next();
    return new VHDLDecimalLiteral(val);
  }
  if(this.lexer.ctoken.ttype == this.lexer.TT_CHAR_LITERAL) {
    let val = this.lexer.ctoken.tvalue;
    this.lexer.next();
    return new VHDLCharacterLiteral(val);
  }
  if(this.lexer.ctoken.ttype == this.lexer.TT_STRING_LITERAL) {
    let val = this.lexer.ctoken.tvalue;
    this.lexer.next();
    return new VHDLStringLiteral(val);
  }
  let nm = this.tryReadName();
  if(nm != null) return nm;

  if(this.lexer.isTokenSymbol('(')) {
    this.lexer.next();
    let ex = this.parseExpression();
    if(!this.lexer.isTokenSymbol(')')) {
      throw new VHDLParserError("Expected ')'.");
    }
    this.lexer.next();
    return ex;
  }
  
  throw new VHDLParserError("Unexpected token: '"+this.lexer.ctoken.tvalue+"'.");
}

VHDLParser.prototype.parseFactor = function() {
  if(this.lexer.isTokenKeyword('abs') || this.lexer.isTokenKeyword('not')) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let term = this.parsePrimary();
    return new VHDLUnaryOperator(chosenOper, term);
  }
  let term = this.parsePrimary();
  if(this.lexer.isTokenOperator('**')) {
    this.lexer.next();
    let term2 = this.parsePrimary();
    return new VHDLMultiplyingOperator("**", term, term2);
  }
  return term;
}

VHDLParser.prototype.parseTerm = function() {
  let term = this.parseFactor();
  while(this.lexer.isTokenSymbol('*')||this.lexer.isTokenSymbol('/')||this.lexer.isTokenKeyword('mod')||this.lexer.isTokenKeyword('rem')) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let term2 = this.parseFactor();
    term = new VHDLMultiplyingOperator(chosenOper, term, term2);
  }
  return term;
}

VHDLParser.prototype.parseSimpleExpression = function() {
  let sign = null;
  if(this.lexer.isTokenSymbol('+')) {
    sign = "+";
    this.lexer.next();
  } else if(this.lexer.isTokenSymbol('-')) {
    sign = "-";
    this.lexer.next();
  }
  let term = this.parseTerm();
  if(sign==="-") term = new VHDLUnaryOperator("-", term);
  while(this.lexer.isTokenSymbol('+')||this.lexer.isTokenSymbol('-')||this.lexer.isTokenSymbol('&')) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let term2 = this.parseTerm();
    term = new VHDLAddingOperator(chosenOper, term, term2);
  }
  return term;
}

VHDLParser.prototype.parseShiftExpression = function() {
  let se = this.parseSimpleExpression();
  if(this.lexer.isTokenKeyword("sll")||this.lexer.isTokenKeyword("srl")||this.lexer.isTokenKeyword("sla")||this.lexer.isTokenKeyword("sra")||this.lexer.isTokenKeyword("rol")||this.lexer.isTokenKeyword("ror")) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let se2 = this.parseSimpleExpression();
    return new VHDLShiftOperator(chosenOper, [se,se2]);
  } else {
    return se;
  }
}

VHDLParser.prototype.parseRelation = function() {
  let se = this.parseShiftExpression();
  if(this.lexer.isTokenOperator("=")||this.lexer.isTokenOperator("/=")||this.lexer.isTokenOperator("<")||this.lexer.isTokenOperator("<=")||this.lexer.isTokenOperator(">")||this.lexer.isTokenOperator(">=")) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let se2 = this.parseShiftExpression();
    return new VHDLComparisonOperator(chosenOper, [se,se2]);
  } else {
    return se;
  }
}

VHDLParser.prototype.parseExpression = function() {
  let rel = this.parseRelation();
  if(this.lexer.isTokenKeyword("and")||this.lexer.isTokenKeyword("or")||this.lexer.isTokenKeyword("xor")||this.lexer.isTokenKeyword("xnor")) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let children = [rel];
    while(true) {
      let rel2 = this.parseRelation();
      children.push(rel2);
      if(!this.lexer.isTokenKeyword(chosenOper)) break;
      this.lexer.next();
    }
    return new VHDLBinaryLogicalOperator(chosenOper, children, true);
  } else if(this.lexer.isTokenKeyword("nand")||this.lexer.isTokenKeyword("nor")) {
    let chosenOper = this.lexer.ctoken.tvalue;
    this.lexer.next();
    let rel2 = this.parseRelation();
    return new VHDLBinaryLogicalOperator(chosenOper, [rel, rel2], false);
  } else {
    return rel;
  }
}

VHDLParser.prototype.parseCondition = function() {
  return this.parseExpression();
}

VHDLParser.prototype.parseConditionalWaveforms = function() {
  let options = [];
  while(true) {
    let wf = this.parseWaveform();
    if(!this.lexer.isTokenKeyword("when")) {
      options.push(new VHDLConditionalWaveform(wf,null));
      break;
    }
    this.lexer.next();
    let cond = this.parseCondition();
    options.push(new VHDLConditionalWaveform(wf,cond));
    if(!this.lexer.isTokenKeyword("else")) {
      break;
    }
    this.lexer.next();
  }
  return options;
}

VHDLParser.prototype.parseAssociationElement = function() {
  this.lexer.saveState();
  let nm = this.tryReadName();
  if(nm != null && this.lexer.isTokenOperator("=>")) {
    this.lexer.next();
    this.lexer.dropState();
  } else {
    this.lexer.restoreState();
    nm = null;
  }
  if(this.lexer.isTokenKeyword("open")) {
    this.lexer.next();
    return {formalPart: nm, actualPart: new VHDLOpenKeyword()};
  }
  let ex = this.parseExpression();
  return {formalPart: nm, actualPart: ex};
}

// Parsiranje jedne naredbe stvaranja primjerka sklopa; ako je argument 'label' različit od
// null, smatra se da je "label:" dio već parsiran i parsira se dalje; ako je 'label' null,
// zahtjeva se da postoji identifikator, dvotocka, pa nastavak i ako ne postoji, prijavljuje
// se greska.
VHDLParser.prototype.parseCompInstStatement = function(label) {
  if(label===null) {
    if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER || this.lexer.checkTokenIsKeyword()) {
        throw new VHDLParserError("Entity instantiation must start with definition of statement label, symbol ':', then ENTITY keyword.");
    }
    label = this.lexer.ctoken.tvalue;
    this.lexer.next();
    if(!this.lexer.isTokenSymbol(':')) {
      throw new VHDLParserError("In entity instantiation, after a statement label, symbol ':' is expected.");
    }
    this.lexer.next();
  }

  // Sada kako god došli ovdje, imamo labelu
  if(this.lexer.isTokenKeyword("component")) {
    throw new VHDLParserError("Entity instantiation using 'COMPONENT' is not supported by this parser.");
  }
  if(this.lexer.ctoken.ttype == this.lexer.TT_SELECTED_NAME || (this.lexer.ctoken.ttype == this.lexer.TT_IDENTIFIER && !this.lexer.checkTokenIsKeyword())) {
    throw new VHDLParserError("Entity instantiation without 'ENTITY' keyword is not supported by this parser.");
  }
  if(!this.lexer.isTokenKeyword("entity")) {
    throw new VHDLParserError("In entity instantiation, after a statement label and symbol ':', 'ENTITY' keyword is expected.");
  }
  this.lexer.next();
  if(this.lexer.ctoken.ttype != this.lexer.TT_SELECTED_NAME) {
    throw new VHDLParserError("In entity instantiation, after 'ENTITY' keyword, a full entity name is expected (for example, work.my_circuit7).");
  }
  let selName = this.lexer.ctoken.tvalue;
  if(selName.length != 2 || selName[0].length<1 || selName[1].length<1) {
    throw new VHDLParserError("In entity instantiation, entity name of form libraryName.entityName was expected; found '"+selName.join(".")+"'.");
  }
  this.lexer.next();
  
  if(!this.lexer.isTokenKeyword("port")) {
    throw new VHDLParserError("In entity instantiation, after entity name, 'PORT' keyword is expected.");
  }
  this.lexer.next();
  if(!this.lexer.isTokenKeyword("map")) {
    throw new VHDLParserError("In entity instantiation, after 'PORT' keyword, 'MAP' keyword is expected.");
  }
  this.lexer.next();

  if(!this.lexer.isTokenSymbol('(')) {
    throw new VHDLParserError("In entity instantiation, after 'MAP' keyword, symbol '(' is expected.");
  }
  this.lexer.next();

  let portMap = [];
  while(true) {
    let ae = this.parseAssociationElement();
    portMap.push(ae);
    if(this.lexer.isTokenSymbol(',')) {
      this.lexer.next();
      continue;
    }
    break;
  }
  if(!this.lexer.isTokenSymbol(')')) {
    throw new VHDLParserError("PORT MAP specification must end with symbol ')'; found '"+this.lexer.ctoken.tvalue+"' instead (are you missing a ','?).");
  }
  this.lexer.next();

  if(!this.lexer.isTokenSymbol(';')) {
    throw new VHDLParserError("Entity instantiation must end with symbol ';'.");
  }
  this.lexer.next();

  return new VHDLCompInstStatement(label, selName, portMap);
}

VHDLParser.prototype.parseArchitectureImpl = function() {
  let statements = [];
  let usedLabels = [];
  while(true) {
    let label = null;
    if(this.lexer.ctoken.ttype == this.lexer.TT_IDENTIFIER && !this.lexer.checkTokenIsKeyword()) {
      let labelCandidate = this.lexer.ctoken.tvalue;
      this.lexer.saveState();
      this.lexer.next();
      if(this.lexer.isTokenSymbol(':')) {
        this.lexer.next();
        this.lexer.dropState();
        label = labelCandidate;
      } else {
        this.lexer.restoreState();
      }
    }
    if(label != null) {
      let lclabel = label.toLowerCase();
      if(usedLabels.indexOf(lclabel) != -1) {
        throw new VHDLParserError("Label '"+label+"' is declared more than once - this is not allowed.");
      }
      usedLabels.push(lclabel);
    }
    if(this.lexer.isTokenKeyword("entity")) {
      if(label===null) {
        throw new VHDLParserError("For entity instantiation, you MUST provide a label before the statement.");
      }
      let stmt = this.parseCompInstStatement(label);
      statements.push(stmt);
      continue;
      //throw new VHDLParserError("Entity instantiation not supported yet.");
    }
    if(this.lexer.isTokenKeyword("with")) {
      throw new VHDLParserError("WITH signal SELECT statement not supported yet.");
    }
    let target = null;
    if(this.lexer.isTokenSymbol('(')) {
      target = this.tryReadSimpleAggregation();
      if(target === null) {
        throw new VHDLParserError("Unexpected token found: '"+this.lexer.ctoken.tvalue+"'.");
      }
    } else {
      if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER || this.lexer.checkTokenIsKeyword()) {
        if(label!=null) {
          throw new VHDLParserError("Unexpected token found: '"+this.lexer.ctoken.tvalue+"'.");
        } else {
          break;
        }
      }
      target = this.tryReadName();
      if(target === null) {
        throw new VHDLParserError("Unexpected token found: '"+this.lexer.ctoken.tvalue+"'.");
      }
    }
    if(!this.lexer.isTokenOperator('<=')) {
      throw new VHDLParserError("Unexpected token found: '"+this.lexer.ctoken.tvalue+"'. Expected assignment operator '<='.");
    }
    this.lexer.next();
    let wfs = this.parseConditionalWaveforms();
    if(!this.lexer.isTokenSymbol(';')) {
      throw new VHDLParserError("Assignment statement must end with ';'. Current unparsed token is '"+this.lexer.ctoken.tvalue+"'.");
    }
    this.lexer.next();
    statements.push(new VHDLConditionalSignalAssignment(label, target, wfs));
  }
  return new VHDLConcurrentStatements(statements);
}

VHDLParser.prototype.parseEntity = function() {
  if(!this.lexer.isTokenKeyword("entity")) {
    throw new VHDLParserError("Entity declaration must start with 'ENTITY' keyword.");
  }
  this.lexer.next();
  if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
    throw new VHDLParserError("After 'ENTITY' keyword, a entity name is expected.");
  }
  let entityName = this.lexer.ctoken.tvalue;
  if(this.lexer.isKeyword(entityName)) {
    throw new VHDLParserError("Entity name can not be keyword.");
  }
  this.lexer.next();
  if(!this.lexer.isTokenKeyword("is")) {
    throw new VHDLParserError("After entity name, a keyword 'IS' is expected.");
  }
  this.lexer.next();

  let ports = null;
  if(this.lexer.isTokenKeyword("port")) {
    ports = this.parsePortDeclaration();
  }

  if(!this.lexer.isTokenKeyword("end")) {
    throw new VHDLParserError("The termination of entity declaration must start with 'END' keyword.");
  }
  this.lexer.next();
  
  if(this.lexer.isTokenKeyword("entity")) {
    this.lexer.next();
  }

  if(this.lexer.ctoken.ttype == this.lexer.TT_IDENTIFIER) {
    let endEntityName =  this.lexer.ctoken.tvalue;
    if(endEntityName != entityName) { 
      throw new VHDLParserError("Entity name used at the begining of declaration ('"+entityName+"') and at end of declaration ('"+endEntityName+"') are not the same!");
    }
    this.lexer.next();
  }

  if(!this.lexer.isTokenSymbol(';')) {
    throw new VHDLParserError("Entity declaration must end with ';'.");
  }
  this.lexer.next();

  return new VHDLEntity(entityName, ports);
}

VHDLParser.prototype.parseArchitecture = function() {
  if(!this.lexer.isTokenKeyword("architecture")) {
    throw new VHDLParserError("Architecture declaration must start with 'ARCHITECTURE' keyword.");
  }
  this.lexer.next();
  if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
    throw new VHDLParserError("After 'ARCHITECTURE' keyword, architecture name is expected.");
  }
  let archName = this.lexer.ctoken.tvalue;
  if(this.lexer.isKeyword(archName)) {
    throw new VHDLParserError("Architecture name can not be keyword.");
  }
  this.lexer.next();
  if(!this.lexer.isTokenKeyword("of")) {
    throw new VHDLParserError("After architecture name, a keyword 'OF' is expected.");
  }
  this.lexer.next();
  if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
    throw new VHDLParserError("After 'ARCHITECTURE "+archName+" OF', entity name is expected.");
  }
  let entityName = this.lexer.ctoken.tvalue;
  if(this.lexer.isKeyword(entityName)) {
    throw new VHDLParserError("Entity name can not be keyword.");
  }
  this.lexer.next();
  if(!this.lexer.isTokenKeyword("is")) {
    throw new VHDLParserError("After entity name, a keyword 'IS' is expected.");
  }
  this.lexer.next();

  let archDeclarativePart = this.parseArchitectureDeclarativePart();

  if(!this.lexer.isTokenKeyword("begin")) {
    throw new VHDLParserError("'BEGIN' was expected.");
  }
  this.lexer.next();

  let stmts = this.parseArchitectureImpl();

  if(!this.lexer.isTokenKeyword("end")) {
    throw new VHDLParserError("'END' was expected.");
  }
  this.lexer.next();

  if(this.lexer.isTokenKeyword("architecture")) {
    this.lexer.next();
  }

  if(this.lexer.ctoken.ttype == this.lexer.TT_IDENTIFIER) {
    let closingArchName = this.lexer.ctoken.tvalue;
    if(archName != closingArchName) {
      throw new VHDLParserError("Architecture name used at the begining of architecture ('"+archName+"') and at the end ('"+closingArchName+"') is not the same!");
    }
    this.lexer.next();
  }

  if(!this.lexer.isTokenSymbol(';')) {
    throw new VHDLParserError("Architecture implementation must end with ';'.");
  }
  this.lexer.next();

  return new VHDLArchitecture(archName, entityName, archDeclarativePart, stmts);
}

VHDLParser.prototype.parseArchitectureDeclarativePart = function() {
  let declarations = [];
  let allNames = [];
  while(true) {
    let kind = null;
    if(this.lexer.isTokenKeyword("signal")) {
      kind = VHDLParser.SC_SIGNAL;
    } else if(this.lexer.isTokenKeyword("constant")) {
      kind = VHDLParser.SC_CONSTANT;
    } else {
      break;
    }
    this.lexer.next();

    if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
      throw new VHDLParserError("After '"+kind.toUpperCase()+"' keyword, name is expected.");
    }
    let names = [];
    while(true) {
      if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
        throw new VHDLParserError("After ',', another specification is expected in '"+kind.toUpperCase()+"' declaration.");
      }
      let name = this.lexer.ctoken.tvalue;
      if(this.lexer.isKeyword(name)) {
        throw new VHDLParserError("'"+kind.toUpperCase()+"' name can not be keyword ('"+name+"').");
      }
      let nameLC = name.toLowerCase();
      if((undefined === allNames.find(function fu(e) {return e===nameLC;})) && (undefined === names.find(function fu(e) {return e.toLowerCase()===nameLC;}))) {
        names.push(name);
      } else {
        throw new VHDLParserError("Name '"+name+"' was already declared.");
      }
      this.lexer.next();
      if(this.lexer.isTokenSymbol(',')) {
        this.lexer.next();
        continue;
      }
      break;
    }
    if(!this.lexer.isTokenSymbol(':')) {
      throw new VHDLParserError("In '"+kind.toUpperCase()+"' declaration, after name(s), a ':' is expected.");
    }
    this.lexer.next();

    let typeSpec = this.tryReadName();
    if(typeSpec===null || !(typeSpec.nodeType===VHDLParser.TN_SIMPLE_NAME || typeSpec.nodeType===VHDLParser.TN_RANGED_NAME)) {
      throw new VHDLParserError("After ':', a type name is expected in '"+kind.toUpperCase()+"' declaration.");
    }
    if(typeSpec.nodeType===VHDLParser.TN_SIMPLE_NAME && typeSpec.name.toLowerCase()==="std_logic_vector") {
      throw new VHDLParserError("In '"+kind.toUpperCase()+"', type '"+typeSpec.name+"' must have range defined.");
    }
    if(typeSpec.nodeType===VHDLParser.TN_RANGED_NAME && typeSpec.name.toLowerCase()==="std_logic") {
      throw new VHDLParserError("In '"+kind.toUpperCase()+"', type '"+typeSpec.name+"' can not have range defined.");
    }
    if(this.lexer.isKeyword(typeSpec.name)) {
      throw new VHDLParserError("'"+kind.toUpperCase()+"' type name can not be keyword ('"+typeSpec.name+"').");
    }
    let initializer = null;
    if(this.lexer.isTokenOperator(":=")) {
      this.lexer.next();
      initializer = this.parseExpression();
    }
    if(!this.lexer.isTokenSymbol(';')) {
      throw new VHDLParserError("'"+kind.toUpperCase()+"' declaration must end with ';'.");
    }
    this.lexer.next();

    for(let i = 0; i < names.length; i++) {
      allNames.push(names[i].toLowerCase());
      declarations.push(new VHDLSigConstDeclaration(kind, names[i], typeSpec, initializer));
    }
  }
  return declarations;
}

VHDLParser.prototype.parseLibsAndUses = function() {
  let libNames = [];
  let useNames = [];
  while(true) {
    if(this.lexer.isTokenKeyword("library")) {
      this.lexer.next();
      if(this.lexer.ctoken.ttype != this.lexer.TT_IDENTIFIER) {
        throw new VHDLParserError("After 'LIBRARY' keyword, library name is expected.");
      }
      if(this.lexer.checkTokenIsKeyword()) {
        throw new VHDLParserError("Library name can not be keyword.");
      }
      let libName = this.lexer.ctoken.tvalue.toLowerCase();
      let ind = libNames.indexOf(libName);
      if(ind!=-1) {
        throw new VHDLParserError("Ne možete istu biblioteku ('"+libName+"') deklarirati više puta.");
      }
      libNames.push(libName);
      this.lexer.next();
      if(!this.lexer.isTokenSymbol(';')) {
        throw new VHDLParserError("Library declaration must end with ';'.");
      }
      this.lexer.next();
      continue;
    }
    if(this.lexer.isTokenKeyword("use")) {
      this.lexer.next();
      if(this.lexer.ctoken.ttype != this.lexer.TT_SELECTED_NAME) {
        throw new VHDLParserError("After 'USE' keyword, use specification is expected.");
      }
      while(true) {
        if(this.lexer.ctoken.ttype != this.lexer.TT_SELECTED_NAME) {
          throw new VHDLParserError("After ',', another specification is expected in USE.");
        }
        let selName = this.lexer.ctoken.tvalue;
        if(libNames.indexOf(selName[0].toLowerCase())==-1) {
          throw new VHDLParserError("Library '"+selName[0]+"' is not defined but 'USE "+selName.join(".")+"' is present.");
        }
        this.lexer.next();
        useNames.push(selName);
        if(this.lexer.isTokenSymbol(',')) {
          this.lexer.next();
          continue;
        }
        break;
      }
      if(!this.lexer.isTokenSymbol(';')) {
        throw new VHDLParserError("USE declaration must end with ';'.");
      }
      this.lexer.next();
      continue;
    }
    break;
  }
  return [libNames, useNames];
}

VHDLParser.prototype.parseLibsAndUsesAndEntity = function() {
  let res1 = this.parseLibsAndUses();
  return [res1[0], res1[1], this.parseEntity()];
}

VHDLParser.prototype.parseDesignUnit = function() {
  let libUses = this.parseLibsAndUses();

  let libNames = libUses[0];
  let useNames = libUses[1];

  let entity = this.parseEntity();
  let architecture = this.parseArchitecture();

  if(entity.entityName != architecture.entityName) {
     throw new VHDLParserError("Using ENTITY, you have defined interface for circuit '"+entity.entityName+"' but then provided architecture for '"+architecture.entityName+"'. Those two identifiers must be the same.");
  }

  return new VHDLDesignUnit(libNames, useNames, entity, architecture);
}

function VHDLPort(name, mode, typeName, range) {
  this.name = name;
  this.mode = mode;
  this.typeName = typeName;
  this.range = range;
}

VHDLPort.prototype.size = function() {
  return this.range==null ? 1 : this.range.size();
}

VHDLPort.MODE_IN = "IN";
VHDLPort.MODE_OUT = "OUT";
VHDLPort.MODE_INOUT = "INOUT";
VHDLPort.MODE_LINKAGE = "LINKAGE";
VHDLPort.MODE_BUFFER = "BUFFER";

function VHDLRange(rangeStart, rangeEnd, rangeDirection) {
  this.rangeStart = rangeStart;
  this.rangeEnd = rangeEnd;
  this.rangeDirection = rangeDirection;
}

VHDLRange.prototype.size = function() {
  return Math.abs(this.rangeStart - this.rangeEnd) + 1;
}

VHDLRange.DIRECTION_TO = "TO";
VHDLRange.DIRECTION_DOWNTO = "DOWNTO";

function VHDLEntity(entityName, ports) {
  this.nodeType = "entity";
  this.entityName = entityName;
  this.ports = ports;
}

VHDLEntity.prototype.numberOfInputBits = function() {
  if(this.ports==null) return 0;
  let n = 0;
  for(let i = 0; i < this.ports.length; i++) {
    let p = this.ports[i];
    if(p.mode===VHDLPort.MODE_IN) {
      if(p.typeName.toLowerCase() === "std_logic") {
        n++;
      } else if(p.typeName.toLowerCase() === "std_logic_vector") {
        n += Math.abs(p.range.rangeStart - p.range.rangeEnd) + 1;
      }
    }
  }
  return n;
}

VHDLEntity.prototype.numberOfOutputBits = function() {
  if(this.ports==null) return 0;
  let n = 0;
  for(let i = 0; i < this.ports.length; i++) {
    let p = this.ports[i];
    if(p.mode===VHDLPort.MODE_OUT) {
      if(p.typeName.toLowerCase() === "std_logic") {
        n++;
      } else if(p.typeName.toLowerCase() === "std_logic_vector") {
        n += Math.abs(p.range.rangeStart - p.range.rangeEnd) + 1;
      }
    }
  }
  return n;
}

VHDLEntity.prototype.getPort = function(name) {
  if(this.ports==null) return null;
  let lookForName = name.toLowerCase();
  for(let i = 0; i < this.ports.length; i++) {
    let p = this.ports[i];
    if(p.name.toLowerCase() === lookForName) return p;
  }
  return null;
}

