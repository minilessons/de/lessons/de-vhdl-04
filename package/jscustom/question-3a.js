// Vidljivi objekti:
// userData - mapa s podatcima koji se perzistiraju na poslužitelju
// sharedUserData - mapa s podatcima koji se perzistiraju na poslužitelju a vidljivi su između svih zadataka...
// questionCompleted - booleova zastavica

// metoda koja se poziva pri inicijalizaciji pitanja; u userData se može upisati sve što je potrebno...
function questionInitialize() {
  userData.enteredText = "";
  userData.textOK = false;
}

// metoda koja se poziva kako bi napravila prikaz potrebne stranice; dobiva kao argument pogled koji je zadao korisnik
function questionRenderView(vid) {
  if(vid=="start") {
    res = {};
    res.options = ['cont'];
    res.questionState = JSON.stringify({enteredText: userData.enteredText});
    res.view = {action: "page:1"};
    return res;
  }
  if(vid=="done") {
    return {
      options: ['cont'],
      questionState: JSON.stringify({}),
      view: {action: "page:3"}
    };
  }
  if(vid=="notdone") {
    return {
      options: ['ret'],
      questionState: JSON.stringify({}),
      view: {action: "page:2"}
    };
  }
  return null;
}

// metoda koja se poziva kako bi se obradio poslan odgovor; prima kljuc (tj. opciju koja je pritisnuta) i objekt s poslanim podatcima
function questionProcessKey(vid, key, sentData) {
  if(vid=="start") {
    userData.enteredText = sentData.enteredText;
    userData.textOK = sentData.textOK;
    if(userData.textOK) {
      return {action: "view:done", completed: true};
    } else {
      return {action: "view:notdone"};
    }
  } else if(vid=="done") {
    if(!questionCompleted) {
      return {action: "view:start"};
    } else {
      return {action: "next"};
    }
  } else if(vid=="notdone") {
    return {action: "view:start"};
  } else {
    return {action: "fail"};
  }
}

